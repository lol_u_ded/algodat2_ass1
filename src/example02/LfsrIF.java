package example02;

public interface LfsrIF {
    /**
     * Method to calculate and return a random number according to the requested algorithm.
     */
    int a5Rand();

    /**
     * Method to return to content of the LFSR1 as an integer array
     */
    int[] getLfsr1();

    /**
     * Method to return to content of the LFSR2 as an integer array
     */
    int[] getLfsr2();

    /**
     * Method to return to content of the LFSR3 as an integer array
     */
    int[] getLfsr3();
}
