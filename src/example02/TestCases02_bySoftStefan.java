package example02;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

//import org.junit.jupiter.api.Timeout;

public class TestCases02_bySoftStefan {

	private final Lfsr lfsr = new Lfsr();

	@Test
//	@Timeout(value = 500, unit = TimeUnit.MILLISECONDS)
	public void testLfsrSizeOfLfsr() {
		final int[] lfsr1 = lfsr.getLfsr1();
		assertNotNull(lfsr1, "LFSR1 was null");
		assertEquals(19, lfsr1.length, "Size mismatch!");

		final int[] lfsr2 = lfsr.getLfsr2();
		assertNotNull(lfsr2, "LFSR2 was null");
		assertEquals(22, lfsr2.length, "Size mismatch!");

		final int[] lfsr3 = lfsr.getLfsr3();
		assertNotNull(lfsr3, "LFSR3 was null");
		assertEquals(23, lfsr3.length, "Size mismatch!");
	}

	@Test
//	@Timeout(value = 500, unit = TimeUnit.MILLISECONDS)
	public void testResultofLfsr() {
		// test the first outputs
		assertEquals(17, lfsr.a5Rand(), "1st random number is wrong!");
		assertEquals(16, lfsr.a5Rand(), "2nd random number is wrong!");
		assertEquals(23, lfsr.a5Rand(), "3rd random number is wrong!");
		assertEquals(14, lfsr.a5Rand(), "4th random number is wrong!");
		assertEquals(13, lfsr.a5Rand(), "5th random number is wrong!");
	}

	@Test
//	@Timeout(value = 100, unit = TimeUnit.MILLISECONDS)
	public void testA5() {
		final int[] expecteds = {17, 4, 27, 13, 7, 1, 23, 22, 2, 2};
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 999; j++) {
				lfsr.a5Rand();
			}
			assertEquals(expecteds[i], lfsr.a5Rand(), ((i + 1) * 1000) + "th random number is wrong: ");
		}
	}

	@Test
	public void testReturnCopyOfArray() {
		final int[] r1 = lfsr.getLfsr1();
		final int[] r2 = lfsr.getLfsr2();
		final int[] r3 = lfsr.getLfsr3();

		Arrays.fill(r1, -1);
		Arrays.fill(r2, 0);
		Arrays.fill(r3, 1);

		assertEquals(17, lfsr.a5Rand(), "Make sure to return a copy of the arrays!");
	}

	@Test
	public void testRegister1() {
		assertArrayEquals(IntStream.rangeClosed(0, 18).toArray(), lfsr.getLfsr1(), "Register1 mismatch!");
		lfsr.a5Rand();
		assertArrayEquals(
				new int[]{30, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17},
				lfsr.getLfsr1(),
				"Register1 mismatch after 1 iteration"
		);
		lfsr.a5Rand();
		assertArrayEquals(
				new int[]{2, 30, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16},
				lfsr.getLfsr1(),
				"Register1 mismatch after 2 iterations"
		);
	}

	@Test
	public void testRegister2() {
		assertArrayEquals(IntStream.rangeClosed(0, 21).toArray(), lfsr.getLfsr2(), "Register2 mismatch!");
		lfsr.a5Rand();
		assertArrayEquals(
				new int[]{6, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20},
				lfsr.getLfsr2(),
				"Register1 mismatch after 1 iteration"
		);
		lfsr.a5Rand();
		assertArrayEquals(
				new int[]{6, 6, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19},
				lfsr.getLfsr2(),
				"Register1 mismatch after 2 iterations"
		);
	}

	@Test
	public void testRegister3() {
		assertArrayEquals(IntStream.rangeClosed(0, 22).toArray(), lfsr.getLfsr3(), "Register3 mismatch!");
		lfsr.a5Rand();
		assertArrayEquals(
				new int[]{11, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21},
				lfsr.getLfsr3(),
				"Register1 mismatch after 1 iteration"
		);
		lfsr.a5Rand();
		assertArrayEquals(
				new int[]{9, 11, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20},
				lfsr.getLfsr3(),
				"Register1 mismatch after 2 iterations"
		);
	}

}
