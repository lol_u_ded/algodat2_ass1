package example02;

import java.util.Arrays;

public class Lfsr implements LfsrIF {

    private final Register lfsr1;
    private final Register lfsr2;
    private final Register lfsr3;

    /**
     * Default constructor
     */
    public Lfsr() {
        this.lfsr1 = new Register(19);
        this.lfsr2 = new Register(22);
        this.lfsr3 = new Register(23);
    }

    /**
     * Method to calculate and return a random number according to the requested algorithm.
     */
    public int a5Rand() {
        int rand = lfsr1.getNext() ^ lfsr2.getNext() ^ lfsr3.getNext();
        lfsr1.shift(
                ((lfsr1.getArr()[18] ^ lfsr1.getArr()[17])
                        ^ lfsr1.getArr()[16])
                        ^ lfsr1.getArr()[13]
        );

        lfsr2.shift(lfsr2.getArr()[21] ^ lfsr2.getArr()[19]);

        lfsr3.shift(
                ((lfsr3.getArr()[22] ^ lfsr3.getArr()[21])
                        ^ lfsr3.getArr()[15])
                        ^ lfsr3.getArr()[7]
        );
        return rand;
    }

    /**
     * Method to return to content of the LFSR1 as an integer array
     */
    public int[] getLfsr1() {
        return lfsr1.getArr();
    }

    /**
     * Method to return to content of the LFSR2 as an integer array
     */
    public int[] getLfsr2() {
        return lfsr2.getArr();
    }

    /**
     * Method to return to content of the LFSR3 as an integer array
     */
    public int[] getLfsr3() {
        return lfsr3.getArr();
    }
}

class Register {

    private final int[] reg;
    private final int size;

    Register(int size) {
        this.reg = new int[size];
        this.size = size;
        fill();
    }

    private void fill() {
        for (int i = 0; i < size; i++) {
            reg[i] = i;
        }
    }

    public int getNext() {
        return reg[size - 1];
    }

    public int[] getArr() {
        return Arrays.copyOf(reg, size);
    }

    public void shift(int newFirst) {
        if (size - 1 >= 0) System.arraycopy(reg, 0, reg, 1, size - 1);
//        for (int i = size - 1; i > 0; i--) {
//            reg[i] = reg[i - 1];
//        }
        reg[0] = newFirst;
    }
}
