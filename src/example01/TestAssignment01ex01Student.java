package example01;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestAssignment01ex01Student {

	@Test
	public void testMCwith2Rects() {
		// area of enclosing rectangle is 50
		// area of embedded rectangles is 2+12=14
		// result is: 50-14=36

		// embedded rectangle 1 is at position (0,0) with a size of 1x2 
		Rectangle rect1 = new Rectangle(0.0f, 0.0f, 1.0f, 2.0f);
		// embedded rectangle 2 is at position (7,1) with a size of 3x4
		Rectangle rect2 = new Rectangle(7.0f, 1.0f, 3.0f, 4.0f);
		Rectangle[] rects = new Rectangle[]{rect1, rect2};

		MonteCarlo mc = new MonteCarlo(10.0f, 5.0f, rects);

		double area = mc.area(10000);
		// for 10k random points the estimated area should already be close to correct result of 36
		assertTrue(area > 30 && area < 40);
		System.out.println("Run 1: Area 2 embedded rectangles (1x2 and 3x4) is " + area);

		// Lets run the algorithm 2 additional times to see that it's a randomized algorithm 
		// (same input (rectangles do not change) but different output)

		System.out.println("\nTestruns 10 - 1 000 000 Shots\n");

		System.out.println("10 shots: ");
		area = mc.area(10);
		System.out.println("Run 2: Area 2 embedded rectangles (1x2 and 3x4) is " + area);

		System.out.println("100 shots: ");
		area = mc.area(100);
		System.out.println("Run 3: Area 2 embedded rectangles (1x2 and 3x4) is " + area);

		System.out.println("1000 shots: ");
		area = mc.area(1000);
		System.out.println("Run 4: Area 2 embedded rectangles (1x2 and 3x4) is " + area);

		System.out.println("10 000 shots: ");
		area = mc.area(10000);
		System.out.println("Run 5: Area 2 embedded rectangles (1x2 and 3x4) is " + area);

		System.out.println("100 000 shots: ");
		area = mc.area(100000);
		System.out.println("Run 6: Area 2 embedded rectangles (1x2 and 3x4) is " + area);

		System.out.println("1 000 000 shots: ");
		area = mc.area(1000000);
		System.out.println("Run 7: Area 2 embedded rectangles (1x2 and 3x4) is " + area);

	}

	@Test
	public void testMCwith0Rects() {
		// area of enclosing rectangle is 50
		// area of embedded rectangles is 0

		Rectangle[] rects = new Rectangle[]{};

		MonteCarlo mc = new MonteCarlo(10.0f, 5.0f, rects);

		double area = mc.area(10);
		assertEquals((int) area, 50);
		System.out.println("Area without embedded rectangles is " + area);

	}
}
