package example01;

import java.util.Random;

public class MonteCarlo implements MonteCarloIF {

	private final Rectangle[] embeddedRectangles;
	private final float length;
	private final float width;

	/*
	 * Constructor
	 * @param length - length of the enclosing rectangle
	 * @param width - width of the enclosing rectangle
	 * @param embeddedRectangles[] - array that contains the embedded rectangles
	 * @throws IllegalArgumentException if embeddedRectangles is null.
	 */
	public MonteCarlo(float length, float width, Rectangle[] embeddedRectangles) {
		if (embeddedRectangles == null) {
			throw new IllegalArgumentException();
		}
		this.embeddedRectangles = embeddedRectangles;
		this.length = length;
		this.width = width;
	}

	public float getArea() {
		return length * width;
	}

	/**
	 * Method to calculate/estimate the area of the enclosing rectangle, which is not covered by
	 * the embedded rectangles.
	 *
	 * @param numOfShots - Number of generated random points whose location (inside/outside) is analyzed. Must be greater than 0.
	 * @throws IllegalArgumentException if numOfShots is not greater than 0.
	 */
	public float area(int numOfShots) {
		if (numOfShots <= 0) {
			throw new IllegalArgumentException();
		}
		int hits = 0;
		Random random = new Random();

		for (int i = 0; i < numOfShots; i++) {
			float randX = random.nextFloat() * (length);
			float randY = random.nextFloat() * (width);

			for (Rectangle e : embeddedRectangles) {
				if (inside(e, randX, randY)) {
					hits++;
				}
			}
		}
		System.out.println(hits + " / " + numOfShots);

		// case no rect inside
		if (hits == 0) {
			return getArea();
		}

		return getArea() * ((float) hits / (float) numOfShots);
	}

	
	/**
	 * Method to determine if a given point is inside the given rectangle.
	 * 
	 * @param rect - Given rectangle 
	 * @param x,y  - Coordinates of the point to check
	 * @throws IllegalArgumentException if rect is null.
	 */
	public Boolean inside(Rectangle rect, float x, float y) {
		if (rect == null) {
			throw new IllegalArgumentException();
		}

		return rect.originX <= x && rect.originY <= y && x > rect.width && y > rect.length;
	}
}
