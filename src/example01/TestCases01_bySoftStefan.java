package example01;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

//import org.junit.jupiter.api.Timeout;

public class TestCases01_bySoftStefan {

	@Test
	public void testMCwith2Rects() throws IllegalAccessException {
		final Rectangle rect1 = new Rectangle(0.0f, 0.0f, 1.0f, 2.0f);
		final Rectangle rect2 = new Rectangle(7.0f, 1.0f, 3.0f, 4.0f);
		final Rectangle[] rects = new Rectangle[]{rect1, rect2};

		final MonteCarlo mc = new MonteCarlo(10.0f, 5.0f, rects);

		double area = mc.area(10000);
		assertEquals(36f, area, 4f, "Area mismatch too big: " + area);

		area = mc.area(10000);
		assertEquals(36f, area, 4f, "Area mismatch too big: " + area);

		area = mc.area(10000);
		assertEquals(36f, area, 4f, "Area mismatch too big: " + area);
	}

	@Test
	// works on my chromebook, WILL work on your PC as well :)
//	@Timeout(value = 6_000, unit = TimeUnit.MILLISECONDS)
	public void testAreaMultipleOverlaps() throws IllegalAccessException {
		// 16
		final Rectangle r1 = new Rectangle(7f, 1f, 4f, 4f);
		// 2
		final Rectangle r2 = new Rectangle(5f, 6f, 2f, 1f);
		// 15 - 4 - 2
		final Rectangle r3 = new Rectangle(3f, 3f, 5f, 3f);

		final MonteCarlo mc = new MonteCarlo(10f, 10f, new Rectangle[]{r1, r2, r3});

		final float area = mc.area(100_000_000);
		assertEquals(73f, area, 0.1f, "Actual area is 73.0, but estimated: " + area);
	}

	@Test
	public void testMCwith0Rects() throws IllegalAccessException {
		final Rectangle[] rects = new Rectangle[]{};

		final MonteCarlo mc = new MonteCarlo(10.0f, 5.0f, rects);

		final float area = mc.area(10);
		assertEquals(50f, area, 0.0001f, "Area has to be exactly 50f");
	}

	@Test
	public void testIllegalArgumentExceptionCtor() {
		assertThrows(IllegalArgumentException.class, () -> new MonteCarlo(5f, 4f, null),
				"IllegalArgumentException has to be thrown on null-array");
		new MonteCarlo(0f, 0f, new Rectangle[0]);
	}

	@Test
	public void testIllegalArgumentExceptionArea() {
		assertThrows(IllegalArgumentException.class, () -> new MonteCarlo(0f, 0f, new Rectangle[0]).area(-1),
				"IllegalArgumentException has to be thrown on negative numbers");
		assertThrows(IllegalArgumentException.class, () -> new MonteCarlo(0f, 0f, new Rectangle[0]).area(0),
				"IllegalArgumentException has to be thrown on 0");
	}

	@Test
	public void testIllegalArgumentExceptionInside() {
		assertThrows(IllegalArgumentException.class, () -> new MonteCarlo(0f, 0f, new Rectangle[0]).inside(null, 0f, 0f),
				"IllegalArgumentException has to be thrown on negative null-Rectangle");
	}

}
